/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * The class contains the basic parameters for the missiles in the SI game.
 * Everything in this class is set through the constructor, all the
 * operations are handled in the Gameplay class.
 * @author Zsolt
 */
public class Missile extends JLabel{
    /**
     * The starting x coordinate for the missile
     */
    private int startX;
    /**
     * The starting y coordinate for the missile
     */
    private int startY;
    /**
     * The direction for the missile.
     */
    private boolean direction;
    /**
     * The dimension of the missile.
     */
    private Dimension d = new Dimension(3,10);
    /**
     * The icon for the missile.
     */
    ImageIcon icon;
    /**
     * The constructor sets the background (just in case it would have to
     * fall back from the image), the size, the visibility and sets the icon
     * depending on the passed in direction parameter.
     * @param x the x coordinate for the missile start point
     * @param y the y coordinate for the missile start point
     * @param direction the direction of the missile (eg Player or Alien)(true/false)
     */
    public Missile(int x, int y, boolean direction){
        this.startX = x;
        this.startY = y;
        this.direction = direction;
        this.setBackground(Color.PINK);
        this.setPreferredSize(d);
        this.setSize(d);
    //    System.out.println("Missile created.");
        this.setLocation(x, y);
        this.setOpaque(true);
        this.setVisible(true);
        if (direction){
            icon = createImageIcon("images/pmissile.png");
            this.setIcon(icon);
            this.setOpaque(false);
        }else{
            icon = createImageIcon("images/amissile.png");
            this.setIcon(icon);
            this.setOpaque(false);
        }
        
    }
            
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
    }

     /** Returns an ImageIcon, or null if the path was invalid. */
    protected ImageIcon createImageIcon(String path) {
    java.net.URL imgURL = getClass().getResource(path);
    if (imgURL != null) {
        return new ImageIcon(imgURL);
    } else {
        System.err.println("Couldn't find file: " + path);
        return null;
    }
}
    
}
