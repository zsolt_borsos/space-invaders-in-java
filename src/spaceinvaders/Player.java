/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 * The class handles everything that is related to the player.
 * @author Zsolt
 */
public class Player extends spaceinvaders.Spacecraft {
    /**returns true when the player lost a life */
    private boolean destroying;
    
    /**
     * the player's score
     */
    private int score;
    /**
     * the lives the player have
     */
    private int lives;
    /**
     * the player's image
     */
    private ImageIcon icon = createImageIcon("images/player.png");
    /**
     * lost life animation
     */
    private ImageIcon destroyed = createImageIcon("images/destroyed2.gif");
    /**
     * the lost health animation
     */
    private ImageIcon gotHit = createImageIcon("images/playerShield.gif");
    
 /**
 * The constructor of the player object with direction set to true.
 * @param x player starting X coordinate
 * @param y player starting Y coordinate
 * @param w player's width
 * @param h player's height
 * @param health player's health
 * @param lives player's lives
 */
    public Player(int x, int y, int w, int h, int health, int lives){
        super(x,y,w,h,health,true);    //initialise object with set "direction = true" as they always shoot up
        this.animationFrameCounterLimit = 35;
        this.lives = lives;
        this.score = 0;
        this.setIcon(icon);
        this.destroying = false;
        this.setOpaque(false);
        System.out.println("Player created.");
        
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
    }
    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {  //need to change it to private later!
        this.lives = lives;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {  //need to change it to private later!
        this.score = score;
    }
    
 /**
 * The method increments the score with @param s
 */
    public void addToScore(int s){
        this.score += s;
    }
    public void addLife(){
        this.lives++;
    }
    public void takeLife(){
        this.lives--;
    }
 /**
 * The method responsible to decide when to take a health or when to take a 
 * life from the user depending on current health. It also sets the animation
 * timers and changes the player object name so it will be invulnerable
 * for a short period of time.
 * 
 */
    public void takeHealth(){
        if (!destroying){
          this.setIcon(gotHit);
          this.setName("playerGotHit");
          this.health--;
          this.animationFrameCounterLimit = 35;
        }
        if (health <= 0){
            takeLife();
            setHealth(3);
            this.destroying = true;
            this.setIcon(destroyed);
            this.animationFrameCounterLimit = 180;
        } 
        
    }
    
    public boolean isDestroying(){
       return destroying;   
    }
    
 /**
 * The method resets the player to "normal" state, changing the name back
 * to player /so collision detection will detect it/ and sets the destroying
 * parameter to false.
 */
    
    public void setPlayerToNormal(){
        
        this.setIcon(icon);
        //repaint();
        this.setName("player");
        this.destroying = false;
    }
    
}  //end of class
