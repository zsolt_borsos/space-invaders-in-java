/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Random;
import javax.swing.*;
import java.applet.*;

/**
 * Game play for SI game. The game mechanism is implemented in this class
 * including alien movement and collision detection.
 * @author Zsolt
 */
public class Gameplay extends JPanel {
    /**
     * The current level
     */
    private int currentLevel;
    /**
     * The current difficulty
     */
    private int difficulty;
    /**
     * Player object.
     */
    Player p;
    /**
     * The amount of pixels the aliens move
     */
    private int step;
    /**
     * Frame counter for messages during the game
     */
    private int gameFrameCounterForMessage;
    /**
     * The amount of alien missiles on the screen
     */
    private int alienMissileCount;
    /**
     * Mute/unmute the game
     */
    private boolean mute;
    /**
     * True if red alien is on the screen
     */
    private boolean isRedAlienSpawned;
    /**
     * flag for alien movement
     */
    private boolean moveAcross;
    /**
     * flag for alien movement
     */
    private boolean moveDown;
    /**
     * flag for clearing the current level
     */
    private boolean win;
    /**
     * flag for losing
     */
    private boolean lose;
    /**
     * flag to stop processing Gameplay object after a lost game
     */
    private boolean gameEnded;
    /**
     * checks the status of object p
     */
    private boolean isPlayerHit;
    /**
     * true if a sound file is playing currently on this object
     */
    private boolean isSoundPlaying; 
    /**
     * true is a message is on screen
     */
    boolean msgShow;
    /**
     * the main counter for the game
     */
    private int gameFrameCounter;
    /**
     * define a dimension that is used for this object
     */
    private final Dimension d = new Dimension(640, 430); // set game dimension
    /**
     * the amount of pixels the missiles travel
     */
    private final int missileStep = 3;
    /**
     * the last row for aliens to reach
     */
    private final int lastRowY = 340; //set the last row aliens can be
    /**
     * alien movement speed
     */
    private int speed;
    /**
     * current row where the aliens are
     */
    private int row;
    /**
     * starting x point for aliens
     */
    private int spawnPointX;    //set start point for aliens
    /**
     * starting y point for aliens
     */
    private int spawnPointY;
    /**
     * the amount of aliens on the screen currently
     */
    private int alienCounter;
    /**
     * this value is added to the player's score after each
     * level and it increments itself.
     */
    private int levelScoreBonus;
    private JLabel levelClearedTxt = new JLabel();
    private JLabel loseTxt = new JLabel();
    private final Dimension txtDim;
    private final Font txtFont;
    private AudioClip shootSound, alienShootSound, alienDestroyedSound, playerDestroyedSound, playerGotHitSound, winSound, loseSound, redAlienSound;
    
    //red alien 
    private Random rnd = new Random();
    /**
     * The amount in pixels the red alien travel
     */
    private int redStep;
    /**
     * Spawn time for red alien
     */
    private int redAlienSpawnTime;
    /**
     * Check if spawn time is set for red alien.
     */
    private boolean isRedAlienSpawnTimeSet;

    /**
     * The constructor sets all the parameters for the game. 
     * @param level the current level
     * @param diff the current difficulty
     */
    
    public Gameplay(int level, int diff)
    {
        
        //setting labels rdy to display when needed
        txtDim = new Dimension(400, 100);
        txtFont = new Font("Arial", Font.BOLD, 60);
        levelClearedTxt.setText("Level cleared");
        levelClearedTxt.setFont(txtFont);
        levelClearedTxt.setBackground(Color.gray);
        levelClearedTxt.setForeground(Color.green);
        levelClearedTxt.setVisible(false);
        levelClearedTxt.setLocation(140, 120);
        loseTxt.setText("You lose");
        loseTxt.setFont(txtFont);
        loseTxt.setBackground(Color.gray);
        loseTxt.setForeground(Color.RED);
        loseTxt.setVisible(false);
        loseTxt.setLocation(200, 130);
                
        //set default start movement and spawn points
        this.moveAcross = true;
        this.moveDown = false;
        this.spawnPointY = 30;
        this.spawnPointX = 70;
        
        //set gameplay settings
        this.step = 1;
        this.speed = 1;
        this.levelScoreBonus = 100;
        
        //set game mechanism settings
        this.row = 0;
        this.alienMissileCount = 0;
        this.alienCounter = 0;
        this.gameFrameCounter = 0;
        this.msgShow = false;
        
        //set panel settings
        this.setLayout(null);
        this.setSize(d);
        this.setPreferredSize(d);
               
        //set default 1st start settings to arguments
        gameEnded = false;
        this.currentLevel = level;
        this.difficulty = diff;
        mute = false;
        System.out.println("New game was created on level " + level + " on difficulty " + diff);
              
        // draw aliens and player on panel
        drawAliens(spawnPointX, spawnPointY);
        drawPlayer();
        
        //  validate and show panel
        this.validate();
        this.setOpaque(true);
        this.setVisible(true);
        
        //creating sound resources
        createSounds();
        isSoundPlaying = false;
        
        //graphics
        
        this.setDoubleBuffered(true);
        
        //set red alien 1st spawn time
        
        redAlienSpawnTime = rnd.nextInt(1000);
    }  // end of constructor
    
    
public boolean isGameEnded(){
        return gameEnded;
}    
    //red alien
    /**
     * Spawn a red alien
     */
    private void spawnRedAlien(){
        
        int myRandom = rnd.nextInt(100);
        if (myRandom > 50){
            Alien ra = new Alien(5, 5, 30, 20, 4);
            ra.setLocation(5, 5);
            ra.setName("redAlien");
            this.add(ra);
            redStep = 1;
            
        }else{
            Alien ra = new Alien(610, 5, 30, 20, 4);
            ra.setLocation(640, 5);
            ra.setName("redAlien");
            this.add(ra);
            redStep = -1;
        }        
        isRedAlienSpawned = true;
        playSound("ra");
        alienCounter++;
        
    }
    /**
     * Set red alien spawn time depending on current gameframe
     */
    private void setRedAlienSpawnTime(){
        if (!isRedAlienSpawnTimeSet){
            redAlienSpawnTime = rnd.nextInt(1000) + gameFrameCounter + 499;
            if (redAlienSpawnTime > 4999){
                redAlienSpawnTime = rnd.nextInt(1000);
            }
            isRedAlienSpawnTimeSet = true;
            
        }
    }
    
    /**
     * Check if red alien is spawned or if the timer is set.
     * Spawns red alien if it is not on screen yet.
     */
    private void refreshRedAlien(){
       if (isRedAlienSpawnTimeSet){ 
        if (redAlienSpawnTime < gameFrameCounter){
            if (!isRedAlienSpawned){
               spawnRedAlien();
               isRedAlienSpawnTimeSet = false;
                System.out.println("spawning red alien.");
            }   
        }
       }
        if (!getRedAlien()){
            isRedAlienSpawned = false;
            
        }
    
}
/**
 * Check if red alien is exists.
 * @return true if red alien is found.
 */
private boolean getRedAlien(){
    Component c;
     for (int i = 0; i < this.getComponentCount(); i++ ){    //checking all components 
            c = this.getComponent(i);   
            if ("redAlien".equals(c.getName())){    // get component if it is the red alien
                return true;
            } 
     }  
    return false;
}

/**
 * Move the red alien on the screen.
 */
private void moveRedAlien(){
    
        Point loc;
        Component c;
        for (int i = 0; i < this.getComponentCount(); i++ ){    //iterating through components
            c = this.getComponent(i); //get current component
            loc = c.getLocation();
            if ("redAlien".equals(c.getName())){                
                c.setLocation((loc.x + redStep),(loc.y));
                if (loc.x + redStep > 640){
                    remove(c);
                    isRedAlienSpawned = false;
                    isRedAlienSpawnTimeSet = false;
                    System.out.println("alien left the screen on right");
                    alienCounter--;
                }
                if (loc.x + redStep < -20){
                    remove(c);
                    isRedAlienSpawned = false;
                    isRedAlienSpawnTimeSet = false;
                    System.out.println("alien left the screen on left");
                    alienCounter--;
                }           
            }
            
        }   //end of for
        
}

    
    //sound declaration
    public void setMute(boolean mute) {
        this.mute = mute;
    }
    
    /**
     * Create AudioClips for the game.
     * @param path the path to the sound file
     * @return this clip object
     */
    private  AudioClip addSoundFile(String path){
        
    java.net.URL soundURL = getClass().getResource(path);
    if (soundURL != null) {
         AudioClip clip = Applet.newAudioClip(soundURL); 
         return clip;
    } else {
        System.err.println("Couldn't find file: " + path);
        return null;
    }
    }
    /**
     * Assign the sound files to variables
     */
    private void createSounds(){
       shootSound = addSoundFile("sound/gun.wav");
       alienShootSound = addSoundFile("sound/shoot.wav");
       alienDestroyedSound = addSoundFile("sound/explosion.wav");
       playerDestroyedSound = addSoundFile("sound/explosion3.wav");
       playerGotHitSound = addSoundFile("sound/playershield.wav");
       winSound = addSoundFile("sound/win1.wav");
       loseSound = addSoundFile("sound/fail1.wav");
       redAlienSound = addSoundFile("sound/ufo_lowpitch.wav");
        
    }
    
    /**
     * Play the sound clip based on a.
     * @param a the string to identify which clip to play
     */
    public void playSound(String a){
        if (!mute){
            switch(a){
                case "ps": shootSound.play();
                    break;
                case "ad" : alienDestroyedSound.play();
                    break;
                case "as" : alienShootSound.play();
                    break;
                case "pd" : playerDestroyedSound.play();
                    break;
                case "ph" : playerGotHitSound.play();
                    break;
                case "ws" : winSound.play();
                    break;
                case "ls" : loseSound.play();
                            this.isSoundPlaying = true;
                            gameFrameCounterForMessage = gameFrameCounter + 600;
                    break;
                case "ra" : redAlienSound.play();
                    break;
                    
            }
        }
    }
    
    //end of sound part
    /**
     * Load next level.
     */
    public void nextLevel(){
       
        System.out.println("next level");
        currentLevel++;
        p.addToScore(levelScoreBonus);
        p.addLife();
        levelScoreBonus += 150; 
        spawnPointY += 10;
        if (spawnPointY >= 70) {
            spawnPointY = 70;
        }
        drawAliens(spawnPointX, spawnPointY);
        row = 0;
        speed = 1;
        gameFrameCounter = 0;
        moveAcross = true;
        moveDown = false;
        alienMissileCount = 0;
        if (step < 0){
            step = step * -1;
        }
        win = false;
        lose = false;
        
        // reset red alien 
        isRedAlienSpawnTimeSet = false;
        isRedAlienSpawned = false;
        
    }
    
    /**
     * Draw the player on the gameplay area
     */
private void drawPlayer(){
       System.out.println("DrawPlayer");
       p = new Player((this.getWidth()/2), (this.getHeight()-30), 30, 20, 3, 3); //drawing players
       p.setName("player");
       this.add(p); //adding player to JPanel
       p.setLocation((this.getWidth()/2), (this.getHeight()-30)); // setting start position       
   }
   
/**
 * Draw aliens on game play area.
 * @param sX aliens start X position
 * @param sY aliens start Y position
 */
private void drawAliens(int sX, int sY){
     //   System.out.println("Drawing Aliens");      //debug
        int type = 1;   //starting type
        sX = spawnPointX;
        sY = spawnPointY;
        int alienRow = 1;
        alienCounter = 0;
        for (int i = 0; i < 5; i++){    //nested for to create 5x11 aliens
            for (int k = 0; k < 11; k++){
                sX += 40;  // increment x starting point so they dont spawn at the end. Maybe put them in the middle?
                Alien alien = new Alien(sX,sY, 30, 20, type); // creating alien object
                alien.setName("alien");
                this.add(alien);    //adding alien to the panel
                alien.setLocation(sX, sY);  //set the location of alien
                alienCounter++;
            } // nested for end
            sY += 30; // increment y so they will start on a new row
            sX = 70;    // reset x so they line up
            if (alienRow == 1 || alienRow == 3){      //only increment type on certain situation (eg 2 lines of 2 types)
            type++;
            }
            alienRow++;  // track row
            
        } // main for end
       this.validate();     //validate panel after new component added
    }
  
/**
 * Count the alien components in gameplay area
 * @return the amount of alien components found
 */
private int updateAlienCounter(){
          
        Component c;
        int counter = 0;
        for (int i = 0; i < this.getComponentCount(); i++ ){    //check all components
            c = this.getComponent(i); //get current component
            if (c instanceof Alien){    // check if it is an alien
                counter++;  // count aliens
            }
        }
       // System.out.println("Alien left on screen: " + counter);
        return counter;
      }

/**
 * Animate player depending on health after it got hit
 */
private void doPlayerHitAction(){
        
        if (!"playerGotHit".equals(p.getName())){           
            p.takeHealth();     //take a health from player
            if (p.isDestroying()){  // if it is destroyed
              playSound("pd");  // play destroyed sound
            }else{  // if it is just a "normal" hit
                playSound("ph");    // play hit sound
            }
        }else{
            checkPlayerStatus();
        }    
}

/**
 * Set player animation back to normal if counter reached limit.
 */
private void checkPlayerStatus(){
    p.animationFrameCounter++;
    if (p.animationFrameCounterLimit < p.animationFrameCounter){
        p.setPlayerToNormal();
        p.animationFrameCounter = 0;
        this.isPlayerHit = false;
    }
    
}
/**
 * The "main" method in class, as most of the things are being called here.
 * See line comments for details. 
 */

    public void refreshGame(){
       
        //to keep calculations fast?, resetting gameframecounter on 5k
        
        gameFrameCounter++;
     //  System.out.println("gameframe: " + gameFrameCounter);
        if (gameFrameCounter > 5000){
         gameFrameCounter = 0;    
        }
        
        // move the missiles on "full speed"/on all frames       
        moveMissiles();
        markDestroyingAliens();
        checkForAnimatedCraftToRemove();  // check animated aliens
        
      // play animation for player
   
      if (isPlayerHit){
        doPlayerHitAction();
      }
      
      // check if player dead or not
      
      if (p.getLives() < 0){
          lose = true;         
      }
        
      // check if player lost
      if (lose){ 
          msgShow = true;   
      }
  
      // check if player win
       if (win){  // if player cleared the level
            gameFrameCounterForMessage = gameFrameCounter + 400;
            msgShow = true;
            win = false;
            clearAliens();
      }    
     // check if any msg needs showing
      
      if (!msgShow){
        alienCounter = updateAlienCounter();
       if (alienCounter > 0){ // if there are still aliens, then do the moves
        
       //red alien part    
       
        refreshRedAlien();
        if (isRedAlienSpawned){
        moveRedAlien();
        }else{
            
          setRedAlienSpawnTime();          
        }
       // red alien ends   
           
           switch (difficulty)     //depending on difficulty, the movement speed is changing
            //some wierd calculation here that i thought it would be good at 1st...
        {
            case 0 :
                    speed = row * 2 - 1;
                    if (speed > 8) speed = 8;
                    if ((gameFrameCounter % (12 - speed)) == 0){                       
                    moveAliens();
                    }
                    break;
            case 1 : 
                    speed = row * 2;
                    if (speed > 8) speed = 8;
                    if ((gameFrameCounter % (10 - (speed))) == 0){
                    moveAliens();
                    }
                    break;
            case 2 : 
                    speed = row +1;
                    if (speed > 6) speed = 6;
                    if ((gameFrameCounter % (8 - speed)) == 0){
                    moveAliens();
                    }
                    break;
        }
        
        }else{  // if no aliens left
            this.win = true;    // write win text on screen
            playSound("ws");    //and play the sound
             
        }
        
        }else{ // show the msg for win/lose
          if (!lose){      
            if (gameFrameCounter < gameFrameCounterForMessage){ // check if msg needs to be shown or not
                this.add(levelClearedTxt);  // show msg
                levelClearedTxt.setPreferredSize(txtDim);
                levelClearedTxt.setSize(txtDim);
                levelClearedTxt.setVisible(true);               
            }else{  // clear msg after it was on screen long enough
              levelClearedTxt.setVisible(false);
              levelClearedTxt.setSize(0,0); 
              msgShow = false;
              nextLevel();  //start next level
              this.add(p);  // add player back the game
            }
          }else{  // if player lose
                
                if (!isSoundPlaying){      //check if the lose sound playing/played or not
                    playSound("ls");    // play it if not
                }
                this.add(loseTxt);  //add lose msg
                loseTxt.setPreferredSize(txtDim);
                loseTxt.setSize(txtDim);
                loseTxt.setVisible(true);
                this.remove(p); //remove player from screen
                if (gameFrameCounterForMessage < gameFrameCounter){ // uf msg was shown long enough signal to GameEngine to stop processing
                    gameEnded = true;
                }
          }
      }
    }
    
    /**
     * Creates a new instance of Missile, plays the sound and adds
     * it to the missile counter.
     * @param x the start x pos
     * @param y the start y pos
     */
    private void shootMissileA(int x, int y){
        Missile am = new Missile(x,y, false);   //create new Missile object
        am.setName("aM");   //as alien missile
        this.add(am);   //add missile to screen
        playSound("as");    // play a sound
        alienMissileCount++;   //increment missile counter // for further use
    }
    
    /**
     * Generate randomness in alien shootings.
     * @param x start pos x
     * @param y start pos y
     */
    private void alienRandomShoot(int x, int y){
    // creating randomness for alien shoots
    Random rg = new Random();
    int r;
        
           switch(alienMissileCount){
               case 0 : r = rg.nextInt(100);
                        if (r > 90){
                            shootMissileA(x,y);
                        }
                   break;
               case 1 :  r = rg.nextInt(100);
                        if (r > 93){
                            shootMissileA(x,y);
                        }
                   break;
               case 2 : r = rg.nextInt(100);
                        if (r > 95){
                            shootMissileA(x,y);
                        }
                   break;
               case 3 : r = rg.nextInt(100);
                        if (r > 97){
                            shootMissileA(x,y);
                        }
                   break;
               case 4 : r = rg.nextInt(100);
                        if (r < 3){
                            shootMissileA(x,y);
                        }
                   break;
           }
        
    }
    
    /**
     * Move player missile component on the gameplay area.
     * @param c the missile component that is being moved.
     * @return the new location
     */
    private Point movePlayerMissile(Component c){
        Point loc;     
        loc = c.getLocation();  //get missile location
        c.setLocation((loc.x),(loc.y-missileStep)); // set new location for missile
        loc.setLocation(loc.x, (loc.y - missileStep));  // record new location
        return loc; //return location for further use
    }
    /**
     * Move alien missile component on the gameplay area.
     * @param c the missile component
     * @return the new position
     */
    private Point moveAlienMissile(Component c){
        Point loc;
        loc = c.getLocation();
        c.setLocation((loc.x),(loc.y+missileStep));
        loc.setLocation(loc.x, (loc.y + missileStep));
      //  repaint();
        return loc;
    }
       
    /**
     * Start the alien destroyed animation
     * @param c the alien component that is being destroyed
     */
    private void alienDestroyedAnimStart(Component c){
       Alien a = (Alien) c; // "convert" component to alien
       a.alienGotHit(); // set flags for destroyed alien object 
       a.alienAnimate();
       a.destroyMe();
       if (gameFrameCounter > 4969){  // check if gameframecounter is close to reset
        a.setAlienDestroyedTimer(10);      // set new timer if it is
       }else{
        a.setAlienDestroyedTimer(gameFrameCounter);  // set default timer. 30 frames for animation
       }
       
        playSound("ad"); // play the sound for alien destroyed
       
    }
    
    /**
     * Collision detection is a complex method. See line comments for more details.
     * @param loc the location where collision detection will be checked
     * @param dir the direction of movement
     * @return true if it hit something
     */
    private boolean collisionDetection(Point loc, boolean dir){  //player missile collision detection
      Component c;
      c = this.getComponentAt(loc.x,loc.y); // get the component from location
      if (dir){ // check for direction 
      if (loc.y > 0){   //if missile is not at the end of the screen
        if (c instanceof Alien){ // if it hit an alien
            getScoredPoints(c);     //get score based on bg color
            alienDestroyedAnimStart(c); //start alien animation
            return true;
        }else{      //check with dimensions as well
                int posHelper;
                for (int i = 0; i < 3; i++){
                    posHelper = i * 5;  // this will be 0, 5, 10. The pixels where it will check for collision
                    c = this.getComponentAt(loc.x,loc.y + posHelper);
                    if (c instanceof Alien){ // if it hit an alien                   
                    getScoredPoints(c);
                    alienDestroyedAnimStart(c);
                    return true;
                    }
                } //end of for
                for (int i = 0; i < 3; i++){
                    posHelper = i * 5;  // this will be 0, 5, 10. The pixels where it will check for collision
                    c = this.getComponentAt(loc.x+3,loc.y+posHelper);  //checking the other side of the missile
                    if (c instanceof Alien){ // if it hit an alien
                    getScoredPoints(c);
                    alienDestroyedAnimStart(c);
                    return true;
                    }
                } //end of for
        } //end of else
      }
      }else{   // if direction is false (eg alien missiles)
         // System.out.println("y: " + loc.y);  
          if (loc.y < 419){
                if (c instanceof Player){ // if it hit the player
                    return true;
                }else{  //check for dimension
                    int posHelper;
                for (int i = 0; i < 3; i++){
                    posHelper = i * 5;
                    c = this.getComponentAt(loc.x,loc.y + posHelper);
                    if (c instanceof Player){ // if it hit player
                       // System.out.println("player got hit..");
                        return true;
                    }
                } //end of for
                for (int i = 0; i < 3; i++){
                    posHelper = i * 5;
                    c = this.getComponentAt(loc.x+3,loc.y+posHelper);
                    if (c instanceof Player){ // if it hit player
                      //  System.out.println("player got hit...");
                        return true;
                    }
                }
                } //end of else
            }
    }
    return false;
  } 
    
    /**
     * Clear "bugged" aliens from the screen.
     */
    private void clearAliens(){
         
        Component c;
        for (int i = 0; i < this.getComponentCount(); i++ ){    //iterating through components
            System.out.println("Components at clearAliens: " + i);
            c = this.getComponent(i); //get current component
                if (c instanceof Alien){   //check for aliens
                    remove(c);
                }
            }
    }
    
    /**
     * Move the missiles on the screen. Complex method, check line comments for details.
     */
    public void moveMissiles(){
    Component c;
    Point loc;
      
    for (int i = 0; i < this.getComponentCount(); i++ ){    //iterating through components
           c = this.getComponent(i); //get current component
                if ("pM".equals(c.getName())){   //check for player missile
                    loc = movePlayerMissile(c);
                    if (loc.y < -10){
                        this.remove(c);  //remove the missile
                        this.p.shooting = false;
                    }
                    if (collisionDetection(loc, true)){ // if it hit something
                        alienCounter--;
                       // System.out.println("i hit something!" + c.toString());
                        this.remove(c);     //remove missile too from gameplay
                        this.p.shooting = false;    //enable player to shoot again                                
                    }
                } // end of pM check
                
               if ("aM".equals(c.getName())){ //check for alien missiles
                   loc = moveAlienMissile(c);
                   if (loc.y > 429){
                       this.remove(c);
                       alienMissileCount--;
                   }
                   if (collisionDetection(loc, false)){ // if it hit something
                        alienMissileCount--;
                        //System.out.println("i hit something!" + c.toString());
                        this.remove(c);     //remove missile too from gameplay                       
                     //   p.takeHealth();     // removed, handled in separate method now.
                        isPlayerHit = true; //set the for player hit
                   }
               }
                
    } //end of for  
}
    
    /**
     * Get scored points based from alien.
     * @param c the alien component
     */
private void getScoredPoints(Component c){
    Alien a = (Alien) c;
    p.addToScore(a.getScore());
}
    
/**
 * Update alien status for animation.
 */
private void markDestroyingAliens(){
    
        Alien a;
        Component c;
        for (int i = 0; i < this.getComponentCount(); i++ ){    //checking all components
            c = this.getComponent(i); //get current component      
            //check for destroying aliens               
             if (c instanceof Alien){
                a = (Alien) c;
                if (a.isDestroyed()){
                    c.setName("destroyingAlien");
                }
            }
        }
}

/**
 * Check for any animated aliens to remove from the screen if their counter
 * reaches the limit
 */
private void checkForAnimatedCraftToRemove(){
    Component c;
    for (int i = 0; i < this.getComponentCount(); i++){
        c = this.getComponent(i);
        
        if (c instanceof Alien){
              //  System.out.println("I am an alien!");
                if ("destroyingAlien".equals(c.getName())){
                  //  System.out.println("I am about to be testroyed.");
                    Alien al = (Alien) c;
                    al.animationFrameCounter++;
                    if (al.animationFrameCounter > al.animationFrameCounterLimit){                       
                        remove(c);
                        
                    } 
                }
          } //end of instanceof if       
    }   //end of for
    
}

/**
 * Move the aliens. Complex method, check line comments for more details.
 */
    private void moveAliens(){
        
   /* removed, refactoring
    * 
            //check for destroying aliens               
             if (c instanceof Alien){
                a = (Alien) c;
                if (a.isDestroyed()){
                    c.setName("destroyingAlien");
                }
            }
            //new animation check end
            
            if (c instanceof Alien){
              //  System.out.println("I am an alien!");
                if ("destroyingAlien".equals(c.getName())){
                  //  System.out.println("I am about to be testroyed.");
                    Alien al = (Alien) c;
                 //   System.out.println("timer: "+ al.getAlienDestroyedTimer());
                 //   System.out.println("gameframe: " + gameFrameCounter);
                 //   System.out.println("name: " + al.getName());
                    if ((gameFrameCounter - al.getAlienDestroyedTimer()) > 20){                       
                        remove(c);
                        
                        //isShortSoundPlaying = false;
                    //    System.out.println("alien removed with new method");
                        System.out.println("timer: "+ al.getAlienDestroyedTimer());
                        System.out.println("gameframe: " + gameFrameCounter);
                        //System.out.println("Calling myself now.");
                        //moveAliens();
                        System.out.println("Need to change i to the next component");
                        System.out.println("i:" + i);
                    }else{
                        System.out.println("moving in destroyed part");
                    c.setLocation((loc.x + step),(loc.y));
                    
                    }
                   
                    if ((al.getAlienDestroyedTimer() - gameFrameCounter) < -50){
                        remove(c);
                        System.out.println("Bugged alien removed!");
                        System.out.println("timer: "+ al.getAlienDestroyedTimer());
                        System.out.println("gameframe: " + gameFrameCounter);
                      }
                      
               }
          }
            //hopefuly the code above is working now instead of this // old version
            if ("destroyingAlien".equals(c.getName())){       
                a = (Alien) c;
                if (a.getAlienDestroyedTimer() < gameFrameCounter){ //check for the animation timer
                    this.remove(c);     //remove it, if it is time
                    a.setAlienDestroyedTimer(-1);   //set timer to -1 to be safe?
                    a = null;   //clearing a                    
                }else{
                    c.setLocation((loc.x + step),(loc.y));
                    
                }
            }     
         
            // check ends
         
            
            if (getComponentCount() < i){
                System.out.println("Movealiens failed. Component number can not be found. i:" +i);
                System.out.println("component count: " + getComponentCount());
                System.out.println("trying to pick the last element from components, probably not right...");
                i = getComponentCount() - 1;  // to make sure array is not out of bound /after deleting components caused bug!
            }
            
            c = this.getComponent(i); //get current component again, in case it was removed before, so the method still useful/works            
            loc = c.getLocation();
    
   * end of removed part. Left here as this did not cause that much lag. For later analysis
   */   
            // System.out.println("Move aliens");
             Point loc;
            // Alien a;
             Component c, u1, u2, u3, u4;
                if (moveAcross){
                    for (int i = 0; i < this.getComponentCount(); i++ ){    //iterating through components
                    c = this.getComponent(i); //get current component
                    loc = c.getLocation();
            
                    if ("alien".equals(c.getName())){       //check for alien
                        if (loc.y < lastRowY){
                            //check for alien below to see if it can shoot or not
                            u1 = this.getComponentAt(loc.x+10, loc.y+31);  
                            if (!"alien".equals(u1.getName())){
                                u2 = this.getComponentAt(loc.x+10, loc.y+61);  //check for alien below 
                                if (!"alien".equals(u2.getName())){
                                    u3 = this.getComponentAt(loc.x+10, loc.y+91);  //check for alien below
                                    if (u3 != null){
                                        if (!"alien".equals(u3.getName())){
                                            u4 = this.getComponentAt(loc.x+10, loc.y+121);  //check for alien below
                                            if (u4 != null){
                                                if (!"alien".equals(u4.getName())){
                                                    if ((alienMissileCount - getDifficulty()) < 3){  //setting max alien missiles depending on difficulty
                                                        alienRandomShoot((loc.x+15),(loc.y+10));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
               
                
                if (loc.x + step > 615){
                moveAcross = false;
                moveDown = true;                
                }
                if (loc.x + step < 6){
                moveAcross = false;
                moveDown = true;                
                }     
               //   System.out.println("moved alien");
                c.setLocation((loc.x + step),(loc.y));
                
                
           }else{   // if aliens are at the last row, then game over
                  lose=true;
                  return;
              }   
           } //end of if alien check
            
          }   //end of for
        } //end of if moveacross
         if (moveDown){  
           for (int i = 0; i < this.getComponentCount(); i++ ){    //iterating trough components
           c = this.getComponent(i); //get current component             
            if ("alien".equals(c.getName())){ 
               loc = c.getLocation(); 
               c.setLocation((loc.x),(loc.y+20));
            }            
            
           }
           moveAcross = true;
           moveDown = false;
           step = step * -1;   
           row++;
        }
        
    } // end of method

    
    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public int getDifficulty() {
        return difficulty;
    }
    
    @Override
    public void paintComponents(Graphics g) {
        super.paintComponents(g); //To change body of generated methods, choose Tools | Templates.
        
    }
    
 }