/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * The class handles everything that is related to the aliens.
 * @param score the score that the player gets when destroying the alien
 * @param icon1 is the alien's image
 * @param destroyAnim the destroyed alien animation
 * @param destroy flags the alien for destruction
 * @param redScores the array of scores the player can get by destroying the red alien
 * @param alienDestroyedTimer the frame limit for alien animations
 * @author Zsolt
 */
public class Alien extends spaceinvaders.Spacecraft {
     
    private int score;
    Color myColor;
    ImageIcon icon1, destroyAnim;
    private boolean gotHit, animate, destroy;
    private int alienDestroyedTimer;
    private final ArrayList<Integer> redScores = new ArrayList();
    
    /**
 * The constructor of the alien object with direction set to false and health to 1
 * @param x alien starting X coordinate
 * @param y alien starting Y coordinate
 * @param w alien's width
 * @param h alien's height
 * @param type an integer that points to an alien type
 */
    public Alien(int x, int y, int w, int h, int type){
        super(x,y,w,h,1,false); //initialise with 1 health and direction false (always shoot down)
        //this.type = type;
        
        //setting animation frame limit
        this.animationFrameCounterLimit = 55;
        
        //adding scores for red alien to arraylist
        redScores.add(50);
        redScores.add(100);
        redScores.add(150);
        redScores.add(250);
        redScores.add(350);
        
        //setting default flags
        gotHit = false;
        animate = false;
        destroy = false;
        //setting destroyed animation image
        destroyAnim = createImageIcon("images/destroyed2.gif");
        
        // create alien based on passed in type
        switch (type){      // to set different image for each here maybe?
            case 1 : greenAlien();
                     System.out.println("Green Alien created.");
                break;
            case 2 : blueAlien();
                     System.out.println("Blue Alien created.");
                break;
            case 3 : yellowAlien();
                     System.out.println("Yellow Alien created.");
                break;
            case 4 : redAlien();
                     System.out.println("Red Alien created.");
                break;
            default: System.out.println("No type was specified.");
              
        }
       // System.out.println("Alien created.");
    }



public int getAlienDestroyedTimer() {
    return alienDestroyedTimer;
}

public void setAlienDestroyedTimer(int alienDestroyedTimer) {
    this.alienDestroyedTimer = alienDestroyedTimer;
}
    
public void alienGotHit(){
    this.gotHit = true;
}
    
/**
 * The method sets the alien icon to play the destroyed animation
 */
public void alienAnimate(){
    this.animate = true;
    this.setIcon(destroyAnim);
    this.setOpaque(false);
    
}

public void destroyMe(){
   // System.out.println("I will be destroyed now. Cya.");
   this.destroy = true;     
}

public boolean isDestroyed(){
    return destroy;
}
/**
 * The method creates a green alien.
 */
private void greenAlien(){
    this.score = 40;
    myColor = Color.GREEN;
    this.setBackground(myColor);
    icon1 = createImageIcon("images/gAlienAnim.gif");
    this.setIcon(icon1);
    this.setOpaque(false);
}

/**
 * The method creates a blue alien
 */
private void blueAlien(){
    this.score = 20;
    myColor = Color.BLUE;
    this.setBackground(myColor);
    icon1 = createImageIcon("images/bluealien.png");
    this.setIcon(icon1);
    this.setOpaque(false);
}

/**
 * The method creates a yellow alien
 */
private void yellowAlien(){
    this.score = 10;
    myColor = Color.YELLOW;
    this.setBackground(myColor);
    icon1 = createImageIcon("images/yellowalien.png");
    this.setIcon(icon1);
    this.setOpaque(false);
}

/**
 * The method creates a red alien with a random score.
 */
private void redAlien(){
    Random myRandom = new Random();
    int scoreId = myRandom.nextInt(5);
    score = redScores.get(scoreId);
    myColor = Color.RED;
    this.setBackground(myColor);
    icon1 = createImageIcon("images/redalien.png");
    this.setIcon(icon1);
    this.setOpaque(false);
}

/**
 * @return the score
 */
public int getScore() {
    return score;
}

@Override
public void paintComponent(Graphics gc){
super.paintComponent(gc);
    
}
    
} // end of class

