/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * This class is an abstraction for the Alien and the Player classes. 
 * It contains the basic behaviours for both player and alien.
 * @author Zsolt
 */
public class Spacecraft extends JLabel{
        
    /**
    * Craft health.
    */
    protected int health;
    
    /**
     * Craft position.
     */
    protected int posX, posY;
    /**
     * frame counter for animation
     */
    protected int animationFrameCounter;
    /**
     * the limit when the animation stops
     */
    protected int animationFrameCounterLimit;
    
    protected boolean shooting, direction;
    
 /**
 * The constructor require the spawn points (x,y), the width
 * and height of the object(h,w) the starting health (health)
 * and the direction (dir).
 */
    Spacecraft(int x, int y, int w, int h, int health, boolean dir)
    {
      
       this.health = health;
       this.direction = dir;
       this.posX = x;
       this.posY = y;
       this.setSize(w, h);
       this.setPreferredSize(new Dimension(w,h));
       this.setOpaque(true);
       this.setVisible(true);
    }
    
    /** 
    * Returns an ImageIcon, or null if the path was invalid. 
    */
    protected ImageIcon createImageIcon(String path) {
    java.net.URL imgURL = getClass().getResource(path);
    if (imgURL != null) {
        return new ImageIcon(imgURL);
    } else {
        System.err.println("Couldn't find file: " + path);
        return null;
    }
}
  
 /**
 * The method creates a new instance of a Missile object. 
 * @param x the X starting coordinate
 * @param y the Y starting coordinate
 * @return the Missile object. 
 */
    public Missile shoot(int x, int y){
        Missile m = new Missile(x, y, getDirection());
        return m;
    }
    
 /**
 * The method sets the X position.
 */
    public void setPosX(int posX) {
        this.posX = posX;
    }

 /**
 * The method sets the Y position.
 */
    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

/**
 * The method returns the starting X position where the missile should appear.
 */
    public int getCannonPosX() {
        int x;
        x = (this.getWidth() / 2) + getPosX();     
        return x;
    }
/**
 * The method returns the starting Y position where the missile should appear.
 */
    public int getCannonPosY() {
        int y;
        y =  getPosY() - (this.getHeight()/ 2);
        return y;
    }
    public boolean getDirection(){
        return direction;
    }
/**
 * The method returns the current health.
 */
    public int getHealth() {
        return health;
    }
/**
 * The method sets the current health.
 */
    public void setHealth(int health) {
        this.health = health;
    }
    
}
