package spaceinvaders;


import java.awt.*; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;


/**
 * The GameEngine class serves as a frame for the Space Invaders game. The frame contains the creation of the main 
 * menu and the empty game play area that will be replaced when a new game is started.
 * The class is also handling the user interaction with mouse, but this should be changed on later version.
 * The frame is responsible for handling the menu requests and to make sure the applet does not start multiple timer threads.
 * The game speed can be tuned
 * @see #createTimer()
 * @see #refreshRate
 * 
 * 
 * @author Zsolt Borsos
 */
public class GameEngine extends javax.swing.JApplet implements Runnable, ActionListener, FocusListener, MouseListener, MouseMotionListener, KeyListener {
                                             
//gameplay

private boolean gameRunning, mute, pause, newGame;
/**  */
private boolean timerStarted;
/** The main timer for the application */
private Timer time;
/** Set the refresh rate for the game (game speed). Change this to tweak game speed (5-10). Default is 10 */
private final int refreshRate = 10;  

private Gameplay myGameObject = null;
//labels
private Label labLevel = new Label("Level:");
private Label labLives = new Label("Lives:");
private Label labDiff = new Label("Difficulty:");
private Label labHealth = new Label("Health:");
private Label labScore = new Label("Score:");
private Label level = new Label();
private Label lives = new Label();
private Label health = new Label();
private Label score = new Label();
private Choice selectDifficulty = new Choice();
//menu buttons
private JButton ngButton;
private JButton sdButton;
private JButton hgButton;
private JButton pgButton;
//menu panels
private JPanel myMainMenu = getMainMenu();
private JPanel mainPanel = getGameFrame();
private JPanel myTopPanel = getTopPanel();
private JPanel myGamePlay = getGameplay();
   


//methods    

/**
 * 
 * The method clears the current #Gameplay object and instantiate a new one with default settings. Difficulty based on
 * the selected difficulty in the dropdown box.
 * 
 * 
 */
private void createNewGame(){
    System.out.println("Creating new game...");    
    if (newGame){
        //clearing game info for a clean start
        if (myGameObject != null){
        mainPanel.remove(myGameObject);
        myGameObject = null;
        repaint();
        }
        //clearing end
        int myDiff = selectDifficulty.getSelectedIndex();
        myGameObject = new Gameplay(1, myDiff);
        myGameObject.setMute(mute);
        myGameObject.setPreferredSize(myGameObject.getPreferredSize());
        myGameObject.setSize(myGamePlay.getSize());
        myGameObject.setBackground(Color.BLACK);
        myGameObject.setOpaque(true);
        myGameObject.setVisible(true);        
        System.out.println("mainPanel valid?" + mainPanel.isValid());
        mainPanel.remove(myGamePlay);
        mainPanel.add(myGameObject, BorderLayout.CENTER);
        mainPanel.validate();
        System.out.println("mainPanel valid?" + mainPanel.isValid());
        newGame = false; 
        gameRunning = true;
        myGameObject.addKeyListener(this);
        resumeGameplay();
        }
    
}

/**
 * 
 * The method sets the mute parameter to false.
 * 
 */
private void unMuteGame(){
    System.out.println("Game is not muted anymore.");
    this.sdButton.setText("Sound: On");
    this.mute = false;
    if (myGameObject != null){
        myGameObject.setMute(mute);
    }
}

/**
 * 
 * The method mutes the game. It sets the mute parameter to true.
 */
private void muteGame(){
    
    System.out.println("Game is muted now.");
    this.mute = true;
    if (myGameObject != null){
        myGameObject.setMute(mute);
    }
    this.sdButton.setText("Sound: Off"); 
} 

/**
 * 
 * The method is not yet implemented.
 * 
 */
private void getHighScoresPanel(){
    System.out.println("High scores here.");
}

/**
 * 
 * The method sets the pause parameter to true.
 * 
 */
private void pauseGameplay(){
    System.out.println("Game is paused.");
    this.pause = true;
    this.pgButton.setText("Resume Game");
}
/**
 * The method sets the pause parameter to false.
 */
private void resumeGameplay(){
    System.out.println("Resuming gameplay.");
    this.pause = false;
    this.pgButton.setText("Pause Game");   
}

/**
 * The method contains the logic for the main menu. 
 * @param e     is the ActionEvent object that is passed from the menu buttons.
 *              Based on e the method decides which method will be called.
 * 
 */
private void menuLogic(ActionEvent e){
    String option = "a";
    if (e != null){
    option = e.getActionCommand();
    }
    
    switch(option){
        case "New Game" :   this.newGame = true;
                            createNewGame();
                            break;
        case "Sound: On" : muteGame();
            break;
        case "Sound: Off" : unMuteGame();
            break;
        case "High Scores" : getHighScoresPanel();
            break;
        case "Pause Game" : pauseGameplay();
            break;
        case "Resume Game" : resumeGameplay();
            break;
        default : System.out.println("def menu option");
        
    }
}
/**
 * The method creates a JPanel object that shows the informations of the game for the user.
 * It contains the labels for the game interface. It also sets an ItemListener for the difficulty selector.
 * @return the JPanel object.
 */
private JPanel getTopPanel(){
     JPanel topPanel = new JPanel(new GridLayout(3, 4));
     topPanel.setSize(640, 120);
     topPanel.setPreferredSize(new Dimension(625, 100));
     topPanel.setFont(new Font("Helvetica", Font.BOLD, 20));
     selectDifficulty.setBackground(Color.WHITE);
     selectDifficulty.add("Easy");
     selectDifficulty.add("Normal");
     selectDifficulty.add("Hard");
     selectDifficulty.addItemListener(new ItemListener() {

         @Override
         public void itemStateChanged(ItemEvent ie) {
             System.out.println("Difficulty changed to: " + selectDifficulty.getSelectedItem());    
         }
     } );
     topPanel.add(labScore);
     topPanel.add(score);
     topPanel.add(labDiff);
     topPanel.add(selectDifficulty);
     topPanel.add(labLives);
     topPanel.add(lives);
     topPanel.add(labLevel);
     topPanel.add(level);
     topPanel.add(labHealth);
     topPanel.add(health);
     topPanel.add(new Label());
     topPanel.add(new Label());
     topPanel.setBackground(Color.WHITE);
     topPanel.setOpaque(true);
     topPanel.setVisible(true);
     return topPanel;
     
 }
 /**
 * The method creates a JPanel that contains the menu buttons. The method also assigns the ActionListeners to the buttons.
 * @return the JPanel object.
 */
 private JPanel getMainMenu() {
     JPanel menu = new JPanel(new GridLayout(1, 4));  //for the menu
     menu.setPreferredSize(new Dimension(640, 50));
     ngButton = new JButton("New Game");
     sdButton = new JButton("Sound: On");
     hgButton = new JButton("High Scores");
     pgButton = new JButton("Pause Game");
     menu.add(ngButton);
     menu.add(sdButton);
     menu.add(hgButton);
     menu.add(pgButton);
     ngButton.addActionListener(this);
     sdButton.addActionListener(this);
     hgButton.addActionListener(this);
     pgButton.addActionListener(this);
     menu.setBorder(BorderFactory.createLineBorder(Color.GRAY, 5));
     
     return menu;
 }
/**
 * The method creates a JPanel that only has a black background to shown on first startup.
 * @return the JPanel object.
 */
private JPanel getGameplay() {
     JPanel gpArea = new JPanel();
     gpArea.setBackground(Color.BLACK);
     gpArea.setPreferredSize(new Dimension(625, 475));
     gpArea.setSize(625, 475);
     return gpArea;
 }

/**
 * The method creates a JPanel with BorderLayout to serve as a frame for the applet.
 * @return the JPanel object.
 */
private JPanel getGameFrame(){
    JPanel gameframe = new JPanel(new BorderLayout());
    gameframe.setPreferredSize(new Dimension(640, 580));
    return gameframe;
}

/**
 * The method creates the GUI, it puts together the game interface.
 */
 private void createGUI() {
     
         //create the gui here
     
     //setting up the main frame for the game
     mainPanel.add(myTopPanel, BorderLayout.NORTH);
     mainPanel.add(myGamePlay, BorderLayout.CENTER);
     this.add(mainPanel, BorderLayout.CENTER);
     this.add(myMainMenu, BorderLayout.NORTH);    
     
} 

 /**
 * The method runs once, when the applet is first loaded. It loads the GUI, sets the listeners for the main window
 * and sets the default game properties.
 */
@Override
public void init() {
     
    setSize(650, 580);
    addMouseListener(this);
    addMouseMotionListener(this);
    addFocusListener(this);
    try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    createGUI();
                    System.out.println("GUI created!");                    
                }
            });
        } catch (Exception e) { 
            System.err.println("GUI failed to load.");
        }
    
    this.setFocusable(true);
    //gameplay related
    gameRunning = false;
    mute = false;
    pause = false;
     
 } 
 

public void paintComponent(Graphics g){
           
}

@Override
 public void update(Graphics g){
    paintComponent(g);  
 }

/**
 * The method is responsible to update the user interface.
 */
private void updateMyHud(){
    if (this.myGameObject.p.getLives() < 0) {
        this.lives.setText("0");
    }else{
        this.lives.setText(String.valueOf(this.myGameObject.p.getLives()));
    }
    this.score.setText(String.valueOf(this.myGameObject.p.getScore()));
    this.level.setText(String.valueOf(this.myGameObject.getCurrentLevel()));
    this.health.setText(String.valueOf(this.myGameObject.p.getHealth()));
    //repaint();
}

/**
 * The method creates the main timer. It serves as a "heart" for the game.
 */

private void createTimer(){
    timerStarted = true;
//    currentTime = System.currentTimeMillis();
    time = new Timer(refreshRate, new ActionListener() {
        
     @Override    
     public void actionPerformed(ActionEvent aae) {   
     if (gameRunning){                  
            if (!pause){
                if (!myGameObject.isGameEnded()){
                    updateMyHud();    
                    myGameObject.refreshGame();
                    if (!myGameObject.isFocusOwner()){
                        myGameObject.requestFocus();
                    } 
                } 
            }// end of pause check
            repaint();
     } //end of gamerunning if 
    } // end of event
    });
}

/**
 * The start method only creates a new timer if there isn't one already running.
 */

@Override
 public void start() { 
 
     if (!timerStarted){  
         createTimer();
     }else{
         
     }
 time.start();
 }
   
/**
 * The method stops the main timer if the user leaves the browser window.
 */

@Override
 public void stop(){  
  time.stop();
 }
 
@Override
 public void run () {  
    
 }

    @Override
    public void actionPerformed(ActionEvent ae) {
 
        System.out.println("Action performed!" + ae.toString());
        menuLogic(ae);
        

   }

    @Override
    public void focusGained(FocusEvent fe) {
        System.out.println("focus gained"); 
    }

    @Override
    public void focusLost(FocusEvent fe) {
      System.out.println("focus lost");
      }

    @Override
    public void mouseClicked(MouseEvent me) {
     
    }
 
 /**
 * The method handles the user interaction with the game play area. This method should be
 * moved as it is not related to the game engine/ it should be implemented in
 * Gameplay or Player class.
 */

    @Override
    public void mousePressed(MouseEvent me) {
    
    if (gameRunning){
        if (!pause){
          if (!myGameObject.msgShow){    
           if (!myGameObject.p.shooting){
           int x = this.myGameObject.p.getX();
           int y = this.myGameObject.p.getCannonPosY();
           int rx = x+15;   //set missile starting point to middle
               if (x > 630){    //check if it is at the end of the screen
                 rx = 630;     
               }
               //if (x  15){     //check if it is at the end of the screen
               //  rx = 20;
               //}
           if (!myGameObject.p.isDestroying()){    // check if player alive
            Missile m = this.myGameObject.p.shoot(rx,y);
            m.setName("pM"); //player missile
            this.myGameObject.add(m);
         //  m.setLocation(this.myGameObject.p.getCannonPosX()-15, this.myGameObject.p.getCannonPosY());
            this.myGameObject.p.shooting = true;
           
          //sound here
            if (!this.mute){
                this.myGameObject.playSound("ps");
            }
          }
              
           this.myGameObject.validate();
        
           } // end of shooting if  
          } //end of msgshow if
        } //end of pause if
       } //gamerunning check
    
    }

    @Override
    public void mouseReleased(MouseEvent me) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseDragged(MouseEvent me) {
       if (!pause){
        int x = me.getX();
        if (x > 630){
            x = 630;
        }
        if (x < 20){
            x = 20;
        }
        showStatus( "Dragged mouse at (" + me.getX() + "," + me.getY() + ")" );
        if (gameRunning){
        myGameObject.p.setLocation((x-15), (myGameObject.getHeight()-30));
        }
       }
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        int x = me.getX();
        showStatus( "Mouse at (" + me.getX() + "," + me.getY() + ")" );
        if (gameRunning){
            if (!pause){
                myGameObject.p.setPosX(x);
                myGameObject.p.setPosY(myGameObject.getHeight()-30);
                if (x > 630){
                    x = 630;
                }
                if (x < 20){
                    x = 20;
                }            
                myGameObject.p.setLocation((x-15), (myGameObject.getHeight()-30)); // x+15 to put it in middle
        
           }    
        }
    }
/**
 * The method checks typed characters on the keyboard. Currently only (p)ause and (m)ute is implemented.
 */

    @Override
    public void keyTyped(KeyEvent ke) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       char c = ke.getKeyChar();
       switch (c){
           case 'p' :  
               if (this.pause){
                resumeGameplay();
               }else{
                pauseGameplay();
               }
               break;
           case 's' :
               if (this.mute){
                 unMuteGame();
               }else{
                 muteGame();
               }
               break;
       }
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
}